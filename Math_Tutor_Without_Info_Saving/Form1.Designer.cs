﻿namespace Math_Tutor_Without_Info_Saving
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.TextBoxLastName = new System.Windows.Forms.TextBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ListBoxFocusArea = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TextBoxFirstName = new System.Windows.Forms.TextBox();
            this.ButtonCreate = new System.Windows.Forms.Button();
            this.ButtonBegin = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TextBoxPassword = new System.Windows.Forms.TextBox();
            this.TextBoxUserName = new System.Windows.Forms.TextBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(263, 138);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 23);
            this.label6.TabIndex = 34;
            this.label6.Text = "Last Name";
            this.label6.Visible = false;
            // 
            // TextBoxLastName
            // 
            this.TextBoxLastName.Location = new System.Drawing.Point(366, 138);
            this.TextBoxLastName.Name = "TextBoxLastName";
            this.TextBoxLastName.Size = new System.Drawing.Size(157, 20);
            this.TextBoxLastName.TabIndex = 21;
            this.TextBoxLastName.Visible = false;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(366, 271);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.ReadOnly = true;
            this.numericUpDown1.Size = new System.Drawing.Size(157, 20);
            this.numericUpDown1.TabIndex = 26;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(263, 235);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 23);
            this.label4.TabIndex = 33;
            this.label4.Text = "Focus Area";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(263, 271);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 23);
            this.label5.TabIndex = 32;
            this.label5.Text = "Difficulty Level";
            // 
            // ListBoxFocusArea
            // 
            this.ListBoxFocusArea.ForeColor = System.Drawing.SystemColors.WindowText;
            this.ListBoxFocusArea.FormattingEnabled = true;
            this.ListBoxFocusArea.Items.AddRange(new object[] {
            "",
            "Addition",
            "Subtraction",
            "Multiplication",
            "Division"});
            this.ListBoxFocusArea.Location = new System.Drawing.Point(366, 235);
            this.ListBoxFocusArea.MaximumSize = new System.Drawing.Size(157, 35);
            this.ListBoxFocusArea.MinimumSize = new System.Drawing.Size(157, 20);
            this.ListBoxFocusArea.Name = "ListBoxFocusArea";
            this.ListBoxFocusArea.Size = new System.Drawing.Size(157, 30);
            this.ListBoxFocusArea.TabIndex = 24;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(263, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 23);
            this.label3.TabIndex = 31;
            this.label3.Text = "First Name";
            this.label3.Visible = false;
            // 
            // TextBoxFirstName
            // 
            this.TextBoxFirstName.Location = new System.Drawing.Point(366, 106);
            this.TextBoxFirstName.Name = "TextBoxFirstName";
            this.TextBoxFirstName.Size = new System.Drawing.Size(157, 20);
            this.TextBoxFirstName.TabIndex = 20;
            this.TextBoxFirstName.Visible = false;
            // 
            // ButtonCreate
            // 
            this.ButtonCreate.Location = new System.Drawing.Point(279, 64);
            this.ButtonCreate.Name = "ButtonCreate";
            this.ButtonCreate.Size = new System.Drawing.Size(93, 23);
            this.ButtonCreate.TabIndex = 28;
            this.ButtonCreate.Text = "&Create Account!";
            this.ButtonCreate.UseVisualStyleBackColor = true;
            this.ButtonCreate.Click += new System.EventHandler(this.ButtonCreate_Click);
            // 
            // ButtonBegin
            // 
            this.ButtonBegin.Location = new System.Drawing.Point(394, 64);
            this.ButtonBegin.Name = "ButtonBegin";
            this.ButtonBegin.Size = new System.Drawing.Size(93, 23);
            this.ButtonBegin.TabIndex = 29;
            this.ButtonBegin.Text = "&Log In!";
            this.ButtonBegin.UseVisualStyleBackColor = true;
            this.ButtonBegin.Click += new System.EventHandler(this.ButtonBegin_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(263, 174);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 23);
            this.label2.TabIndex = 27;
            this.label2.Text = "UserName";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(263, 206);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 23);
            this.label1.TabIndex = 25;
            this.label1.Text = "Password";
            // 
            // TextBoxPassword
            // 
            this.TextBoxPassword.Location = new System.Drawing.Point(366, 206);
            this.TextBoxPassword.Name = "TextBoxPassword";
            this.TextBoxPassword.Size = new System.Drawing.Size(157, 20);
            this.TextBoxPassword.TabIndex = 23;
            // 
            // TextBoxUserName
            // 
            this.TextBoxUserName.Location = new System.Drawing.Point(366, 174);
            this.TextBoxUserName.Name = "TextBoxUserName";
            this.TextBoxUserName.Size = new System.Drawing.Size(157, 20);
            this.TextBoxUserName.TabIndex = 22;
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("Rockwell", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 19;
            this.listBox1.Items.AddRange(new object[] {
            "Welcome to Ultimate Agile Math Student Pro Learner Awesome Turbo Extreme Virtual " +
                "Tutor!",
            "",
            "Please Login or Create an Account for the Individual Student:"});
            this.listBox1.Location = new System.Drawing.Point(55, -33);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(701, 80);
            this.listBox1.TabIndex = 30;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(811, 352);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TextBoxLastName);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ListBoxFocusArea);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TextBoxFirstName);
            this.Controls.Add(this.ButtonCreate);
            this.Controls.Add(this.ButtonBegin);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TextBoxPassword);
            this.Controls.Add(this.TextBoxUserName);
            this.Controls.Add(this.listBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TextBoxLastName;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox ListBoxFocusArea;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TextBoxFirstName;
        public System.Windows.Forms.Button ButtonCreate;
        private System.Windows.Forms.Button ButtonBegin;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox TextBoxPassword;
        public System.Windows.Forms.TextBox TextBoxUserName;
        private System.Windows.Forms.ListBox listBox1;
    }
}

