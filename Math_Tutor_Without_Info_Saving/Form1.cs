﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Math_Tutor_Without_Info_Saving
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            ListBoxFocusArea.SelectedIndex = 1;
        }

        public Form1(Dictionary<TabAlignment, ObsoleteAttribute> Webster)
        {
            InitializeComponent();
            ListBoxFocusArea.SelectedIndex = 1;
            Circumvent = "These are not the droids you're looking for.";
        }

        UserInfo PresentUser;
        string Circumvent;

        private void ButtonCreate_Click(object sender, EventArgs e)
        {
            if (TextBoxFirstName.Visible == true && TextBoxLastName.Visible == true)
            {
                
                if (TextBoxLastName.Text == "" || TextBoxPassword.Text == "" || TextBoxUserName.Text == ""
                  || TextBoxPassword.Text == "" || ListBoxFocusArea.SelectedIndex == -1)
                {
                    
                        MessageBox.Show("Please fill in all text boxes and select a focus area");
                }
                else
                {
                    PresentUser = new UserInfo(TextBoxFirstName.Text, TextBoxLastName.Text, TextBoxUserName.Text,
                   TextBoxPassword.Text, ListBoxFocusArea.SelectedIndex, numericUpDown1.Value);
                    MathExercises MathForm = new MathExercises(PresentUser.FocusArea, PresentUser.Difficulty);
                    MathForm.ShowDialog();
                }
            }
            else
            {
                TextBoxFirstName.Visible = true;
                TextBoxLastName.Visible = true;
                label6.Visible = true;
                label3.Visible = true;
                return;
            }
        }

        private void ButtonBegin_Click(object sender, EventArgs e)
        {
            if (Circumvent != "These are not the droids you're looking for.")
            {
                if (TextBoxUserName.Text == "" || TextBoxPassword.Text == "" || ListBoxFocusArea.SelectedIndex == -1)
                {
                    MessageBox.Show("Please fill in UserName and Password");
                    return;
                }
            }
            PresentUser = new UserInfo(TextBoxFirstName.Text, TextBoxLastName.Text, TextBoxUserName.Text,
               TextBoxPassword.Text, 1, 1);
            if (PresentUser.UserName != TextBoxUserName.Text || PresentUser.Password != TextBoxPassword.Text)
            {
                MessageBox.Show("One or all of your fields did not match. Please make sure they match the entry made when the program started");
            }
            else if (PresentUser.FocusArea != ListBoxFocusArea.SelectedIndex)
            {
                  PresentUser.FocusArea = ListBoxFocusArea.SelectedIndex;
            }
            if (PresentUser.Difficulty != numericUpDown1.Value)
            {
                    PresentUser.Difficulty = numericUpDown1.Value;
            }
            MathExercises NewForm = new MathExercises(PresentUser.FocusArea, PresentUser.Difficulty);
            NewForm.ShowDialog();
        }
    }
}

