﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Math_Tutor_Without_Info_Saving
{
    public partial class LayoutAnswerVisually : Form
    {
        double DoubleDifference;
        int IntDifference;
        int FocusArea;
        decimal Difficulty;
        Integers Jazz;
        Operands Opera;
        public LayoutAnswerVisually(int _FocusArea, decimal _Difficulty, Operands _Opera, 
            Integers _Jazz )
        {
            InitializeComponent();
            FocusArea = _FocusArea;
            Jazz = _Jazz;
            Opera = _Opera;
            Difficulty = _Difficulty;
            if (Difficulty < 3)
            {
            IntDifference = PositiveIntDifference();
            }
            else if (Difficulty == 3)
            {
                DoubleDifference = PositiveDoubleDifference();
            }
        }

        private void ButtonPreviousScreen_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LayoutAnswerVisually_Load(object sender, EventArgs e)
        {
            switch (FocusArea)
            {
                case 1:
                    switch (Difficulty)
                    {
                        case 3m:
                    if (Opera.DoubleRightAnswer < Opera.DoubleUserAnswer)
                            {
                                for (int i = 0; i < DoubleDifference; i++)
                                {
                                    listBox1.Items.Add(Opera.DoubleUserAnswer);
                                    Opera.DoubleUserAnswer--;
                                    listBox1.Items.Add(" - 1 =" + Opera.DoubleUserAnswer);
                                }
                            }
                            else if (Opera.DoubleRightAnswer > Opera.DoubleUserAnswer)
                    {
                        for (int i = 0; i < DoubleDifference; i++)
                        {
                            listBox1.Items.Add(Opera.DoubleUserAnswer);
                            Opera.DoubleUserAnswer++;
                                    listBox1.Items.Add(" + 1 = " + Opera.DoubleUserAnswer);
                                }
                            }
                            break;
                        default:
                            if (Jazz.IntRightAnswer < Jazz.IntUserAnswer)
                            {
                                for (int i = 0; i < IntDifference; i++)
                                {
                                    listBox1.Items.Add(Jazz.IntUserAnswer);
                                    Jazz.IntUserAnswer--;
                                    listBox1.Items.Add($" - 1 = {Jazz.IntUserAnswer}");
                                }
                                listBox1.Items.Add($" = {Jazz.IntUserAnswer - 1}");
                            }
                            else if (Jazz.IntRightAnswer > Jazz.IntUserAnswer)
                            {
                                for (int i = 0; i < IntDifference; i++)
                                {
                                    listBox1.Items.Add(Jazz.IntUserAnswer);
                                    Jazz.IntUserAnswer++;
                                    listBox1.Items.Add($" + = {Jazz.IntUserAnswer}");
                                }
                                listBox1.Items.Add($" = {Jazz.IntUserAnswer + 1}");
                            }
                            break;
            }
                    break;

                case 2:
                    switch (Difficulty)
                    {
                        case 3m:
                            if (Opera.DoubleRightAnswer < Opera.DoubleUserAnswer)
                            {
                                for (int i = 0; i < DoubleDifference; i++)
                                {
                                    listBox1.Items.Add(Opera.DoubleUserAnswer);
                                    Opera.DoubleUserAnswer--;
                                    listBox1.Items.Add(" - 1 =" + Opera.DoubleUserAnswer);
                                }
                                listBox1.Items.Add($" = {Opera.DoubleUserAnswer - 1}");
                            }
                            else if (Opera.DoubleRightAnswer > Opera.DoubleUserAnswer)
                            {
                                for (int i = 0; i <= DoubleDifference; i++)
                                {
                                    listBox1.Items.Add(Opera.DoubleUserAnswer);
                                    Opera.DoubleUserAnswer++;
                                    listBox1.Items.Add(" + 1 =" + Opera.DoubleUserAnswer);
                                }
                                listBox1.Items.Add($" = {Opera.DoubleUserAnswer + 1}");
                            }
                            break;
                        default:
                            if (Jazz.IntRightAnswer < Jazz.IntUserAnswer)
                            {
                                for (int i = 0; i <= IntDifference; i++)
                                {
                                    listBox1.Items.Add(Jazz.IntUserAnswer);
                                    Jazz.IntUserAnswer--;
                                    listBox1.Items.Add($" - 1 = {Jazz.IntUserAnswer}");

                                }
                                listBox1.Items.Add($" = {Jazz.IntUserAnswer - 1}");
                            }
                            else if (Jazz.IntRightAnswer > Jazz.IntUserAnswer)
                            {
                                for (int i = 0; i < IntDifference; i++)
                                {
                                    listBox1.Items.Add(Jazz.IntRightAnswer);
                                    Jazz.IntRightAnswer--;
                                    listBox1.Items.Add(" - 1 = " + Jazz.IntRightAnswer);
                                }
                                listBox1.Items.Add($" ={Jazz.IntUserAnswer - 1}");
                            }
                            break;
                    }
                    break;
                case 3:
                    switch (Difficulty)
                    {
                        case 3m:
                                for (int i = 0; i < Opera.B; i++)
                                {
                                    listBox1.Items.Add(Opera.A);
                                    listBox1.Items.Add(" +");
                                    Opera.A += Opera.A;
                                }
                            listBox1.Items.Add($" = {Opera.DoubleRightAnswer} \n");
                            listBox1.Items.Add($"\n Another way of saying {Opera.A} * {Opera.B}  is  {Opera.B} {Opera.B} times.");

                            break;
                        default:
                            for (int i = 0; i < Jazz.Z; i++)
                            {
                                listBox1.Items.Add(Jazz.A);
                                listBox1.Items.Add(" +");
                                Jazz.A += Jazz.A;
                            }
                            listBox1.Items.Add($" = {Jazz.IntRightAnswer} \n");
                            listBox1.Items.Add($"\n Another way of saying {Jazz.A} * {Jazz.Z}  is  {Jazz.A} {Jazz.Z} times.");
                            break;
                    }
                    break;
                case 4:
                    switch (Difficulty)
                    {
                        case 3m:
                    for (int i = 0; i < Opera.B; i++)
                        {
                            listBox1.Items.Add(Opera.DoubleRightAnswer);
                            Opera.DoubleRightAnswer += Opera.DoubleRightAnswer;
                                listBox1.Items.Add(" + = " + Opera.DoubleRightAnswer);
                        }
                            listBox1.Items.Add($" = {Opera.A} \n");
                            listBox1.Items.Add($"\n Another way of saying {Opera.A} / {Opera.B}  is  {Opera.A} broken into {Opera.B} parts.");

                            break;
                        default:
                            for (int i = 0; i < Jazz.Z; i++)
                            {
                                listBox1.Items.Add(Jazz.IntRightAnswer);
                                Jazz.IntRightAnswer += Jazz.IntRightAnswer;
                                listBox1.Items.Add(" + = " + Jazz.IntRightAnswer);
                            }
                            listBox1.Items.Add($" ={Jazz.A}");
                            listBox1.Items.Add($"\n Another way of saying {Jazz.A} / {Jazz.Z}  is  {Jazz.A} broken into {Jazz.Z} parts.");

                            break;
                    }
                    break;
            }
        }
        private double PositiveDoubleDifference()
        {
            DoubleDifference = (Opera.DoubleRightAnswer - Opera.DoubleUserAnswer);
            if (DoubleDifference < 0)
            {
                DoubleDifference *= -1;
            }
            return DoubleDifference;
        }

        private int PositiveIntDifference()
        {
            IntDifference = (Jazz.IntRightAnswer - Jazz.IntUserAnswer);
            if (IntDifference < 0)
            {
                IntDifference *= -1;
            }
            return IntDifference;
        }
    }

    
}
