﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Math_Tutor_Without_Info_Saving
{
    class UserInfo
    {

            private string _FirstName;
            private string _LastName;
            private string _UserName;
            private string _Password;
            private int _FocusArea;
            private decimal _Difficulty;

            public UserInfo(string FirstName, string LastName, string UserName, string Password, int FocusArea, decimal Difficulty)
            {
                _FirstName = FirstName;
                _LastName = LastName;
                _UserName = UserName;
                _Password = Password;
                _FocusArea = FocusArea;
                _Difficulty = Difficulty;
            }

            public string FirstName { get { return _FirstName; } set { _FirstName = value; } }
            public string LastName { get { return _LastName; } set { _LastName = value; } }
            public string UserName { get { return _UserName; } set { _UserName = value; } }
            public string Password { get { return _Password; } set { _Password = value; } }
            public int FocusArea { get { return _FocusArea; } set { _FocusArea = value; } }
            public decimal Difficulty { get { return _Difficulty; } set { _Difficulty = value; } }

    }
}

