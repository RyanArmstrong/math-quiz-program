﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Math_Tutor_Without_Info_Saving
{
   public class Operands
    {
            
        public double A { get; set; }
        public double B { get; set; }

        public double DoubleRightAnswer { get; set; }
        public double DoubleUserAnswer { get; set; }

        public double DoubleAddition()
        {
            return A + B;
        }

        public double DoubleSubtraction()
        {
            return A - B;
        }

        public double DoubleMultiplication()
        {
            return A * B;
        }

        public double DoubleDivision()
        {
            return A / B;
        }

    }
}
