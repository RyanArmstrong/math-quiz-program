﻿namespace Math_Tutor_Without_Info_Saving
{
    partial class MathExercises
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelAnswer = new System.Windows.Forms.Label();
            this.LabelOperator = new System.Windows.Forms.Label();
            this.LabelOperand2 = new System.Windows.Forms.Label();
            this.LabelOperand1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.ButtonSubmit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LabelAnswer
            // 
            this.LabelAnswer.Location = new System.Drawing.Point(84, 168);
            this.LabelAnswer.Name = "LabelAnswer";
            this.LabelAnswer.Size = new System.Drawing.Size(109, 78);
            this.LabelAnswer.TabIndex = 11;
            this.LabelAnswer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LabelOperator
            // 
            this.LabelOperator.Location = new System.Drawing.Point(125, 22);
            this.LabelOperator.Name = "LabelOperator";
            this.LabelOperator.Size = new System.Drawing.Size(20, 20);
            this.LabelOperator.TabIndex = 10;
            this.LabelOperator.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LabelOperand2
            // 
            this.LabelOperand2.Location = new System.Drawing.Point(166, 14);
            this.LabelOperand2.Name = "LabelOperand2";
            this.LabelOperand2.Size = new System.Drawing.Size(74, 40);
            this.LabelOperand2.TabIndex = 9;
            this.LabelOperand2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LabelOperand1
            // 
            this.LabelOperand1.Location = new System.Drawing.Point(45, 14);
            this.LabelOperand1.Name = "LabelOperand1";
            this.LabelOperand1.Size = new System.Drawing.Size(74, 40);
            this.LabelOperand1.TabIndex = 8;
            this.LabelOperand1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(87, 72);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 7;
            // 
            // ButtonSubmit
            // 
            this.ButtonSubmit.Location = new System.Drawing.Point(96, 124);
            this.ButtonSubmit.Name = "ButtonSubmit";
            this.ButtonSubmit.Size = new System.Drawing.Size(75, 23);
            this.ButtonSubmit.TabIndex = 6;
            this.ButtonSubmit.Text = "&Submit";
            this.ButtonSubmit.UseVisualStyleBackColor = true;
            this.ButtonSubmit.Click += new System.EventHandler(this.ButtonSubmit_Click_1);
            // 
            // MathExercises
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.LabelAnswer);
            this.Controls.Add(this.LabelOperator);
            this.Controls.Add(this.LabelOperand2);
            this.Controls.Add(this.LabelOperand1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.ButtonSubmit);
            this.Name = "MathExercises";
            this.Text = "MathExercises";
            this.Load += new System.EventHandler(this.MathExercises_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label LabelAnswer;
        public System.Windows.Forms.Label LabelOperator;
        public System.Windows.Forms.Label LabelOperand2;
        public System.Windows.Forms.Label LabelOperand1;
        public System.Windows.Forms.TextBox textBox1;
        public System.Windows.Forms.Button ButtonSubmit;
    }
}