﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Math_Tutor_Without_Info_Saving
{
    public class Integers
    {
        public int A { get; set; }
        public int Z { get; set; }

        public int IntRightAnswer { get; set; }
        public int IntUserAnswer { get; set; }

        public Integers()
        {
            
        }

        public int IntSubtraction()
        {
            return A - Z;
        }
        public int IntMultiplication()
        {
            return A * Z;
        }
        public int IntDivision()
        {
            return A / Z;
        }
        public int IntAddition()
        {
            return A + Z;
        }




    }
}
