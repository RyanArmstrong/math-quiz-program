﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime;

namespace Math_Tutor_Without_Info_Saving
{
    public partial class MathExercises : Form
    {
        int FocusArea;
        decimal Difficulty;
        public MathExercises(int _FocusArea, decimal _Difficulty)
        {
            InitializeComponent();
            FocusArea = _FocusArea;
            Difficulty = _Difficulty;
        }

        public MathExercises()
        {
            switch (DoubleWrongAnswers.Count)
            {
                case 1:
                    {
                        doubles = DoubleWrongAnswers.Dequeue();
                        break;
                    }
                default:
                    Dictionary<TabAlignment, ObsoleteAttribute> MessageinABottle = new Dictionary<TabAlignment, ObsoleteAttribute>();
                    if (MessageBox.Show("Would you like some more Math Problems?", "End or Continue Session", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        Form1 NewForm = new Form1(MessageinABottle);
                        NewForm.ButtonCreate.Visible = false;
                        NewForm.label1.Visible = false;
                        NewForm.label2.Visible = false;
                        NewForm.TextBoxPassword.Visible = false;
                        NewForm.TextBoxUserName.Visible = false;
                        this.Close();
                        NewForm.ShowDialog();
                        this.Close();
                    }
                    else
                    {
                        Application.Exit();
                    }
                    break;
            }
            
                {

            }
        }

        Random rand = new Random();
        Integers integers = new Integers();
        Operands doubles = new Operands();
        int counter = 1;

        Queue<Operands> DoubleWrongAnswers = new Queue<Operands>();
        Queue<Integers> IntWrongAnswers = new Queue<Integers>();


        

        private void MathExercises_Load(object sender, EventArgs e)
        {
            {
                switch (FocusArea)
                {
                    case 1:
                        LabelOperator.Text = "+";
                        switch (Difficulty)
                        {
                            case 1m:
                                integers.A = rand.Next(11);
                                integers.Z = rand.Next(11);
                                LabelOperand1.Text = integers.A.ToString();
                                LabelOperand2.Text = integers.Z.ToString();
                                integers.IntRightAnswer = integers.IntAddition();
                                break;
                            case 2m:
                                integers.A = rand.Next(10, 99);
                                integers.Z = rand.Next(10, 99);
                                LabelOperand1.Text = integers.A.ToString();
                                LabelOperand2.Text = integers.Z.ToString();
                                integers.IntRightAnswer = integers.IntAddition();
                                break;
                            case 3m:
                                doubles.A = rand.NextDouble() * 100;
                                doubles.B = rand.NextDouble() * 100;
                                doubles.A -=  doubles.A % .02;
                                doubles.B -= doubles.B % .02;
                                LabelOperand1.Text = doubles.A.ToString();
                                LabelOperand2.Text = doubles.B.ToString();
                                doubles.DoubleRightAnswer = doubles.DoubleAddition();
                                break;
                            default:
                                break;

                        }
                        break;
                    case 2:
                        LabelOperator.Text = "-";
                        switch (Difficulty)
                        {
                            case 1m:
                                integers.A = rand.Next(11);
                                integers.Z = rand.Next(11);
                                LabelOperand1.Text = integers.A.ToString();
                                LabelOperand2.Text = integers.Z.ToString();
                                if (integers.IntSubtraction() < 0)
                                {
                                    integers.IntRightAnswer = (integers.IntSubtraction() * -1);
                                }
                                integers.IntRightAnswer = integers.IntSubtraction();
                                break;
                            case 2m:
                                integers.A = rand.Next(10, 99);
                                integers.Z = rand.Next(10, 99);
                                LabelOperand1.Text = integers.A.ToString();
                                LabelOperand2.Text = integers.Z.ToString();
                                if (integers.IntSubtraction() < 0)
                                {
                                    integers.IntRightAnswer = (integers.IntSubtraction() * -1);
                                }
                                integers.IntRightAnswer = integers.IntSubtraction();
                                break;
                            case 3m:
                                doubles.A = rand.NextDouble() * 100;
                                doubles.B = rand.NextDouble() * 100;
                                doubles.A -= doubles.A % .02;
                                doubles.B -= doubles.B % .02;
                                LabelOperand1.Text = doubles.A.ToString();
                                LabelOperand2.Text = doubles.B.ToString();
                                doubles.DoubleRightAnswer = doubles.DoubleSubtraction();
                                break;
                            default:
                                break;

                        }
                        break;
                    case 3:
                        LabelOperator.Text = "X";
                        switch (Difficulty)
                        {
                            case 1m:
                                integers.A = rand.Next(11);
                                integers.Z = rand.Next(11);
                                LabelOperand1.Text = integers.A.ToString();
                                LabelOperand2.Text = integers.Z.ToString();
                                integers.IntRightAnswer = integers.IntMultiplication();
                                break;
                            case 2m:
                                integers.A = rand.Next(10, 99);
                                integers.Z = rand.Next(10, 99);
                                LabelOperand1.Text = integers.A.ToString();
                                LabelOperand2.Text = integers.Z.ToString();
                                integers.IntRightAnswer = integers.IntMultiplication();
                                break;
                            case 3m:
                                doubles.A = rand.NextDouble() * 100;
                                doubles.B = rand.NextDouble() * 100;
                                doubles.A -= doubles.A % .02;
                                doubles.B -= doubles.B % .02;
                                LabelOperand1.Text = integers.A.ToString();
                                LabelOperand2.Text = integers.Z.ToString();
                                doubles.DoubleRightAnswer = doubles.DoubleMultiplication();
                                break;
                            default:
                                break;

                        }
                        break;
                    case 4:
                        LabelOperator.Text = "/";
                        switch (Difficulty)
                        {
                            case 1m:
                                integers.A = rand.Next(11);
                                integers.Z = rand.Next(11);
                                while (integers.Z == 0 ||integers.A % integers.Z != 0)
                                {
                                    integers.A = rand.Next(11);
                                    integers.Z = rand.Next(11);

                                }
                                LabelOperand1.Text = integers.A.ToString();
                                LabelOperand2.Text = integers.Z.ToString();
                                integers.IntRightAnswer = integers.IntDivision();
                                break;
                            case 2m:
                                integers.A = rand.Next(10, 99);
                                integers.Z = rand.Next(10, 99);
                                while (integers.Z == 0 || integers.A % integers.Z != 0)
                                {
                                    integers.A = rand.Next(11);
                                    integers.Z = rand.Next(11);

                                }
                                LabelOperand1.Text = integers.A.ToString();
                                LabelOperand2.Text = integers.Z.ToString();
                                integers.IntRightAnswer = integers.IntDivision();
                                break;
                            case 3m:
                                doubles.A = rand.NextDouble() * 100;
                                doubles.B = rand.NextDouble() * 100;
                                doubles.A -= doubles.A % .02;
                                doubles.B -= doubles.B % .02;
                                while (doubles.B == 0 || doubles.A % (doubles.B * .01) != 0)
                                {
                                    doubles.A = rand.Next(11);
                                    doubles.B = rand.Next(11);
                                }
                                LabelOperand1.Text = doubles.A.ToString();
                                LabelOperand2.Text = doubles.B.ToString();
                                doubles.DoubleRightAnswer = doubles.DoubleDivision();
                                break;
                            default:
                                break;

                        }
                        break;
                    default:
                        break;
                }
            }
        }
 

        private void ButtonSubmit_Click_1(object sender, EventArgs e)
        {


            if (textBox1.Text != "")
            {

                if (LabelOperand1.Text == doubles.A.ToString())//meaning we're working with Double type rather than int
                {
                    if (double.TryParse(textBox1.Text, out double DoubleUserAnswer))
                        doubles.DoubleUserAnswer = DoubleUserAnswer;
                    if (DoubleUserAnswer == doubles.DoubleRightAnswer)
                    {
                        LabelAnswer.Text = doubles.DoubleRightAnswer.ToString();
                        MessageBox.Show("That's right!", "Celebration");
                        MathExercises SndForm = new MathExercises();
                    }
                    else if (counter < 2)
                    {
                        counter++;
                        MessageBox.Show("That is not the correct answer; try again");
                        textBox1.Clear();
                    }
                    else if (counter == 2)
                    {
                        DoubleWrongAnswers.Enqueue(doubles);
                        textBox1.Clear();
                        LayoutAnswerVisually Show = new LayoutAnswerVisually(FocusArea, Difficulty, doubles, integers);
                        Show.Show();
                        counter = 0;
                    }
                }
                else if (LabelOperand1.Text == integers.A.ToString())//meaning we're working with an int Type rather than a double
                {
                    if (int.TryParse(textBox1.Text, out int IntUserAnswer))
                        integers.IntUserAnswer = IntUserAnswer;
                    if (IntUserAnswer == integers.IntRightAnswer)
                    {
                        LabelAnswer.Text = integers.IntRightAnswer.ToString();
                        MessageBox.Show("That's right!", "Celebration");
                        MathExercises SndForm = new MathExercises();
                        this.Close();
                        SndForm.ShowDialog();
                    }
                    else if (counter < 2)
                    {
                        counter++;
                        textBox1.Clear();
                        MessageBox.Show("That is not the correct answer; try again");
                    }
                    else if (counter == 2)
                    {
                        IntWrongAnswers.Enqueue(integers);
                        textBox1.Clear();
                        LayoutAnswerVisually Show = new LayoutAnswerVisually(FocusArea, Difficulty, doubles, integers);
                        Show.ShowDialog();
                        counter = 0;
                    }
                }
            }
            else
            {
                MessageBox.Show("Please enter an answer in the text box");
            }
        }
    }
}
